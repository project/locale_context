<?php
// $Id$
/**
 * Thomas Gielfeldt <thomas@gielfeldt.com>
 *
 * @file
 *
 * This file contains the functions for the administration interface
 *
 * Copyright 2011
 */

// -------- CALLBACKS --------
/**
 * Context settings page
 *
 * @param array $form_state
 *   FAPI form state
 * @return array
 *   FAPI form definition
 */
function locale_context_settings_form(&$form_state) {
  $form = array();
  $form['locale_context'] = array(
    '#title' => t('Context'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => -127,
  );
  $form['locale_context']['locale_context_available_context'] = array(
    '#type' => 'textfield',
    '#title' => t('Available context'),
    '#description' => t('A comma separated list of contexts available to the system'),
    '#default_value' => implode(',', array_keys(variable_get('locale_context_available_context', array()))),
  );
  $form['locale_context']['locale_context_default_context'] = array(
    '#type' => 'textfield',
    '#title' => t('Default context'),
    '#description' => t('A comma separated list of the default contexts to use'),
    '#default_value' => implode(',', variable_get('locale_context_default_context', array())),
  );
  $form = system_settings_form($form);

  // Put our submit handler first so we can modify the variables before save
  array_unshift($form['#submit'], 'locale_context_settings_form_submit');

  return $form;
}

function locale_context_settings_form_submit($form, &$form_state) {
  // Convert from csv to array
  $contexts = array_filter(array_unique(explode(',', $form_state['values']['locale_context_available_context'])));
  $form_state['values']['locale_context_available_context'] = array_combine($contexts, $contexts);
  $form_state['values']['locale_context_default_context'] = array_filter(array_unique(explode(',', $form_state['values']['locale_context_default_context'])));
}

/**
 * Translate string form (cloned from locale.inc)
 */
function locale_context_translate_edit_form(&$form_state, $lid, $context) {
  // Is this a valid context?
  $contexts = locale_context_available_contexts();
  if (!isset($contexts[$context])) {
    drupal_not_found();
    exit;
  }

  // Fetch source string, if possible.
  $source = db_fetch_object(db_query('SELECT source, textgroup, location FROM {locales_source} WHERE lid = %d', $lid));
  if (!$source) {
    drupal_set_message(t('String not found.'), 'error');
    drupal_goto('admin/build/translate/search');
  }

  // Add original text to the top and some values for form altering.
  $form = array(
    'original' => array(
      '#type'  => 'item',
      '#title' => t('Original text'),
      '#value' => check_plain(wordwrap($source->source, 0)),
    ),
    'lid' => array(
      '#type'  => 'value',
      '#value' => $lid
    ),
    'textgroup' => array(
      '#type'  => 'value',
      '#value' => $source->textgroup,
    ),
    'location' => array(
      '#type'  => 'value',
      '#value' => $source->location
    ),
    'context' => array(
      '#type'  => 'value',
      '#value' => $context,
    ),
  );

  // Include default form controls with empty values for all languages.
  // This ensures that the languages are always in the same order in forms.
  $languages = language_list();
  $default = language_default();
  // We don't need the default language value, that value is in $source.
  $omit = $source->textgroup == 'default' ? 'en' : $default->language;
  unset($languages[($omit)]);
  $form['translations'] = array('#tree' => TRUE);
  // Approximate the number of rows to use in the default textarea.
  $rows = min(ceil(str_word_count($source->source) / 12), 10);
  foreach ($languages as $langcode => $language) {
    $form['translations'][$langcode] = array(
      '#type' => 'textarea',
      '#title' => t($language->name),
      '#rows' => $rows,
      '#default_value' => '',
    );
  }

  // Fetch translations and fill in default values in the form.
  $result = db_query("SELECT DISTINCT translation, language FROM {locale_context_target} WHERE lid = %d AND language <> '%s' AND context = '%s'", $lid, $omit, $context);
  while ($translation = db_fetch_object($result)) {
    $form['translations'][$translation->language]['#default_value'] = $translation->translation;
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save translations'));
  return $form;
}

/**
 * Submit handler for translation edit
 *
 * @param array $form
 *   Form definition
 * @param array $form_state
 *   Form state
 */
function locale_context_translate_edit_form_submit($form, &$form_state) {
  $lid = $form_state['values']['lid'];

  if ($form['form_id']['#value'] == 'locale_context_translate_edit_form') {
    // Load core locale functions
    include_once './includes/locale.inc';

    $context = $form_state['values']['context'];
    foreach ($form_state['values']['translations'] as $key => $value) {
      $translation = db_result(db_query("SELECT translation FROM {locale_context_target} WHERE lid = %d AND language = '%s' AND context = '%s'", $lid, $key, $context));
      if (!empty($value)) {
        // Only update or insert if we have a value to use.
        if (!empty($translation)) {
          db_query("UPDATE {locale_context_target} SET translation = '%s' WHERE lid = %d AND language = '%s' AND context = '%s'", $value, $lid, $key, $context);
        }
        else {
          db_query("INSERT INTO {locale_context_target} (lid, translation, language, context) VALUES (%d, '%s', '%s', '%s')", $lid, $value, $key, $context);
        }
      }
      elseif (!empty($translation)) {
        // Empty translation entered: remove existing entry from database.
        db_query("DELETE FROM {locale_context_target} WHERE lid = %d AND language = '%s' AND context = '%s'", $lid, $key, $context);
      }

      // Force JavaScript translation file recreation for this language.
      _locale_invalidate_js($key);
    }
    drupal_set_message(t('The string has been saved.'));

    $form_state['redirect'] = 'admin/build/translate/search';

    // Clear js cache
    _locale_invalidate_js();
  }

  // Clear locale context cache.
  $source = db_result(db_query("SELECT source FROM {locales_source} WHERE lid = %d", $lid));
  $cache_key = strlen($source) > 255 ? md5($source) : $source;
  cache_clear_all($cache_key, 'cache_locale_context');

  return;
}
